FROM alpine:edge
LABEL maintainer="Duncan Bellamy <dunk@denkimushi.com>"
ENV url=HeinleinSupport/olefy/master/olefy.py
ARG DAPK

RUN addgroup olefy 2>/dev/null \
&& echo 'https://dl-cdn.alpinelinux.org/alpine/edge/testing' >> /etc/apk/repositories \
&& adduser -D --gecos "olefy scanner" --ingroup olefy olefy 2>/dev/null \
&& apk add -u --no-cache openssl gzip py3-oletools py3-magic

# if DAPK is not set all installed packages are returned
RUN echo "DAPK is: $DAPK" \
&& apk list -q $DAPK | awk '{print $1}'> /etc/apkvers \
&& cat /etc/apkvers \
&& ls -lah /etc/apkvers


WORKDIR /home/olefy/
COPY --chown=olefy:olefy profile profile

WORKDIR /usr/local/bin
COPY container-scripts/set-timezone.sh \
container-scripts/health-nc.sh \
entrypoint.sh ./

SHELL [ "/bin/ash", "-o", "pipefail", "-c" ]
RUN wget -q -S https://raw.githubusercontent.com/$url 2>&1 | grep "ETag:" \
| sed -e s+\"++g -e 's+.*ETag:\ ++' > /etc/githash \
&& chmod +x olefy.py

CMD [ "entrypoint.sh" ]
EXPOSE 10050

HEALTHCHECK --start-period=60s CMD health-nc.sh PING 10050 PONG || exit 1
